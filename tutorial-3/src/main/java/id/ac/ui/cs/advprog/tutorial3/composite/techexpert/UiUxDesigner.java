package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class UiUxDesigner extends Employees {
    //TODO Implement
    public UiUxDesigner(String name, double salary) {
        if (salary < 90000) throw new IllegalArgumentException();
        this.name = name;
        this.salary = salary;
        this.role = "UI/UX Designer";
    }

    public double getSalary() {
        return salary;
    }

}
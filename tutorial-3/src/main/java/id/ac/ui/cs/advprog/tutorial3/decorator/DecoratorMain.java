import id.ac.ui.cs.advprog.tutorial3.decorator.Food;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class DecoratorMain {

    public static void main(String args[]) {
        Food burger = BreadProducer.THICK_BUN.createBreadToBeFilled();
        Food beef = FillingDecorator.BEEF_MEAT.addFillingToBread(burger);
        Food cheese = FillingDecorator.CHEESE.addFillingToBread(beef);
        Food cucumber = FillingDecorator.CUCUMBER.addFillingToBread(cheese);
        Food tomato = FillingDecorator.TOMATO_SAUCE.addFillingToBread(cucumber);

        System.out.println(tomato.getDescription());
        System.out.println(tomato.cost());

    }
}

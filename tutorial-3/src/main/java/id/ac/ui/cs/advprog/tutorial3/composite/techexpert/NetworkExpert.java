package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class NetworkExpert extends Employees {
    //TODO Implement
    public NetworkExpert(String name, double salary) {
        if (salary < 50000) throw new IllegalArgumentException();
        this.name = name;
        this.salary = salary;
        this.role = "Network Expert";
    }

    public double getSalary() {
        return salary;
    }

}
package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class CompositeMain {
    public static void main(String args[]) {
        Company company = new Company();

        Ceo aryo = new Ceo("Aryo", 500000.00);
        company.addEmployee(aryo);

        Cto steve = new Cto("Steve", 320000.00);
        company.addEmployee(steve);

        BackendProgrammer frans = new BackendProgrammer("Frans", 94000.00);
        company.addEmployee(frans);

        FrontendProgrammer james = new FrontendProgrammer("James",66000.00);
        company.addEmployee(james);

        UiUxDesigner jonas = new UiUxDesigner("Jonas", 177000.00);
        company.addEmployee(jonas);

        NetworkExpert andy = new NetworkExpert("Andy", 83000.00);
        company.addEmployee(andy);

        for (Employees x : company.getAllEmployees()) {
            System.out.println(x.getName() + " as " + x.getRole());
        }

        System.out.println("Total Net Salaries: " + company.getNetSalaries());
    }
}

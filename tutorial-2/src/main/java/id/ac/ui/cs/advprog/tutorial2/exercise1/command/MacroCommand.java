package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        // TODO Complete me!
        commands.forEach(Command::execute);
    }

    @Override
    public void undo() {
        // TODO Complete me!
        new LinkedList<>(commands).descendingIterator().forEachRemaining(Command::undo);
    }
}
